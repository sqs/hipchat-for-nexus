# HipChat for Nexus plugin

Notify one or more HipChat rooms when artifacts matching a pattern are uploaded. This is a plugin for Sonatype Nexus, tested against version 2.8.1-01.

# Installation

1. Download the most recent ``hipchat-for-nexus-x.x-bundle.zip`` from
https://bitbucket.org/tpettersen/hipchat-for-nexus/downloads
1. Extract ``hipchat-for-nexus-x.x-bundle.zip`` into ``$NEXUS_WORK/plugin-repository/`` (``$NEXUS_WORK`` may be
equivalent to ``sonatype-work/nexus`` if not otherwise defined).
1. Create a **Notification** token from the [HipChat API Tokens](https://www.hipchat.com/docs/api/auth) page.
1. Look up the **API ID**s of the HipChat rooms you wish to notify from the **Rooms** list in HipChat Group Admin.
1. Create a configuration file at ``$NEXUS_WORK/conf/hipchat.json`` with your auth token and mappings
between your artifact patterns and rooms. See the **Configuration** section below for details.
1.

# Configuration

## Minimum

At a minimum, you will need to specify an ``authToken`` and at least one mapping between an Ant glob pattern and a room
id.

For example:

    {
      "authToken": "cafebabecafebabecafebabecafeba",
      "patterns": {
        "*.jar": ["625183"]
      }
    }

## Advanced

You can also customize:

- **color** - ``yellow``, ``green``, ``red``, ``purple``, ``gray`` or ``random``
- **fromName** - the name displayed as the source of the message
- **apiBaseUrl** - if you are running HipChat [on your own hardware](https://www.hipchat.com/server)

For example:

    {
      "authToken": "cafebabecafebabecafebabecafeba",
      "patterns": {
        "*.jar": ["625183", "626922"],
        "**/hipchat/*.xml": ["625183"]
      },
      "color": "red",
      "fromName": "Nellie Nexus",
      "apiBaseUrl": "https://hipchat.mycompany.com/"
    }

## Patterns

The patterns are [Ant globs](https://ant.apache.org/manual/dirtasks.html#patterns), matched against the artifacts path
in your nexus repository. An example of a full path is:

``/com/atlassian/nexus/plugins/hipchat-for-nexus/1.0-SNAPSHOT/hipchat-for-nexus-1.0-20140731.204225-31.jar``

So to match all Maven-style JAR artifacts with the **groupId** ``com.atlassian.nexus.plugins`` and the **artifactId**
``hipchat-for-nexus`` you might use the pattern:

``/com/atlassian/nexus/plugins/hipchat-for-nexus/**/hipchat-for-nexus-*.jar``

Or to match all ZIP artifacts with **groupId** starting with ``com.atlassian`` you might use:

``/com/atlassian/**/*.zip``

For convenience, if your pattern doesn't start with a ``/`` one is automatically prepended, as all artifacts paths start with a ``/``.

Additionally, if your pattern doesn't contain a ``/`` at all, it is automatically prepended with ``/**/`` so it will match on just
the final path segment of the artifact. This is why ``*.jar`` in the example configuration above works.