package com.atlassian.nexus.plugins.hipchat;

import com.google.common.eventbus.Subscribe;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.util.AntPathMatcher;
import org.sonatype.nexus.events.EventSubscriber;
import org.sonatype.nexus.proxy.events.RepositoryItemEventStoreCreate;
import org.sonatype.nexus.proxy.events.RepositoryItemEventStoreUpdate;
import org.sonatype.nexus.proxy.item.StorageItem;
import org.sonatype.sisu.goodies.common.ComponentSupport;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

/**
 *
 */
@Named
@Singleton
public class ItemStoreListener extends ComponentSupport implements EventSubscriber {

    private final HipChatNotifier notifier;
    private final HipChatConfig config;
    private final AntPathMatcher pathMatcher;

    @Inject
    public ItemStoreListener(HipChatNotifier notifier, HipChatConfig config) {
        this.notifier = notifier;
        this.config = config;
        this.pathMatcher = new AntPathMatcher();
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onItemCreate(RepositoryItemEventStoreCreate itemCreate) {
        log.debug("Item created: " + itemCreate.getItem().getName());

        for (String room : resolveRooms(itemCreate.getItem())) {
            sendMessage(itemCreate.getItem(), room, "New artifact!");
        }
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onItemUpdate(RepositoryItemEventStoreUpdate itemUpdate) {
        log.debug("Item updated: " + itemUpdate.getItem().getName());

        for (String room : resolveRooms(itemUpdate.getItem())) {
            sendMessage(itemUpdate.getItem(), room, "Updated artifact!");
        }
    }

    private void sendMessage(StorageItem item, String room, String prefix) {
        String url = getDownloadUrl(item);
        if (StringUtils.isBlank(url)) {
            notifier.send(room, prefix + " " + item.getName(), false);
        } else {
            String message = String.format("%s <a href=\"" + url + "\">%s</a>",
                    prefix, escapeHtml(item.getName()));
            notifier.send(room, message, true);
        }
    }

    private String getDownloadUrl(StorageItem item) {
        // TODO is there a better way to construct the download URL?
        return (String) item.getResourceStoreRequest().getRequestContext().get("request.url");
    }

    private Iterable<String> resolveRooms(StorageItem item) {
        if (config.isValid()) {
            for (Map.Entry<String, Collection<String>> entry : config.roomPatterns().asMap().entrySet()) {
                if (pathMatcher.matches(entry.getKey(), item.getPath())) {
                    return entry.getValue();
                }
            }
        }
        return Collections.emptyList();
    }

}
