package com.atlassian.nexus.plugins.hipchat;

import org.sonatype.sisu.goodies.common.ComponentSupport;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 *
 */
@Named
@Singleton
public class HipChatNotifier extends ComponentSupport {

    private final HipChatConfig config;

    @Inject
    public HipChatNotifier(HipChatConfig config) {
        this.config = config;
    }

    private static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e); // shouldn't happen
        }
    }

    public void send(String room, String message, boolean html) {
        if (!config.isValid()) return;

        URL url;
        try {
            StringBuilder sb =
                    new StringBuilder(config.apiBaseUrl())
                            .append("v1/rooms/message")
                            .append("?room_id=").append(urlEncode(room))
                            .append("&from=").append(urlEncode(config.fromName()))
                            .append("&message=").append(urlEncode(message))
                            .append("&message_format=").append(html ? "html" : "text")
                            .append("&color=").append(urlEncode(config.color()))
                            .append("&auth_token=").append(urlEncode(config.authToken()));

            url = new URL(sb.toString());
        } catch (MalformedURLException e) {
            log.error("Bad HipChat url", e);
            throw new RuntimeException(e); // shouldn't happen
        }

        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            int responseCode = conn.getResponseCode();
            if (responseCode < 200 || responseCode > 299) {
                log.error("Failed to notify HipChat, received " + responseCode);
            }
        } catch (IOException e) {
            log.error("Failed to notify HipChat", e);
        }
    }

}
