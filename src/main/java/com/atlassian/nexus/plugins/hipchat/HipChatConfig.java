package com.atlassian.nexus.plugins.hipchat;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.sonatype.nexus.configuration.application.ApplicationConfiguration;
import org.sonatype.sisu.goodies.common.ComponentSupport;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 */
@Named
@Singleton
public class HipChatConfig extends ComponentSupport {

    private static final String CONFIG_FILE = "hipchat.json";
    private static final String DEFAULT_BASE_URL = "https://api.hipchat.com";
    private static final String DEFAULT_FROM_NAME = "Nexus";
    private static final String DEFAULT_COLOR = "purple";

    private final Config config;

    @Inject
    public HipChatConfig(ApplicationConfiguration applicationConfiguration) {
        File configFile = new File(applicationConfiguration.getConfigurationDirectory(), CONFIG_FILE);
        if (configFile.exists()) {
            config = parseConfig(configFile);
        } else {
            log.error("Missing config file " + configFile.getAbsolutePath());
            config = null;
        }
    }

    public boolean isValid() {
        return config != null;
    }

    public String apiBaseUrl() {
        return config.apiBaseUrl();
    }

    public String authToken() {
        return config.authToken();
    }

    public String fromName() {
        return config.fromName();
    }

    public String color() {
        return config.color();
    }

    public Multimap<String, String> roomPatterns() {
        return config.roomPatterns();
    }

    private Config parseConfig(File configFile) {
        JSONObject json;
        try {
            json = new JSONObject(IOUtils.toString(new FileInputStream(configFile)));
        } catch (IOException e) {
            log.error("Failed to read from " + configFile.getAbsolutePath(), e);
            return null;
        } catch (JSONException e) {
            log.error("Failed to parse JSON from " + configFile.getAbsolutePath(), e);
            return null;
        }

        // required props
        String authToken = json.optString("authToken");
        if (StringUtils.isBlank(authToken)) {
            log.error("authToken not specified in " + configFile.getAbsolutePath());
            return null;
        }

        // optional props
        String apiBaseUrl = json.optString("apiBaseUrl", DEFAULT_BASE_URL);
        if (!apiBaseUrl.endsWith("/")) apiBaseUrl += "/";
        String fromName = json.optString("fromName", DEFAULT_FROM_NAME);
        String color = json.optString("color", DEFAULT_COLOR);

        // parse room patterns
        Multimap<String, String> patternRooms = HashMultimap.create();
        JSONObject patterns;
        try {
            patterns = json.getJSONObject("patterns");
        } catch (JSONException e) {
            log.error("patterns not specified in " + configFile.getAbsolutePath(), e);
            return null;
        }
        for (String pattern : JSONObject.getNames(patterns)) {
            JSONArray rooms;
            try {
                rooms = patterns.getJSONArray(pattern);
            } catch (JSONException e) {
                log.error("Expected JSON array for key " + pattern + " in " + configFile.getAbsolutePath(), e);
                continue;
            }
            for (int i = 0; i < rooms.length(); i++) {
                try {
                    patternRooms.put(normalizePattern(pattern), rooms.getString(i));
                } catch (JSONException e) {
                    log.error("Expected JSON string for " + pattern + "[" + i + "] in " + configFile.getAbsolutePath(), e);
                }
            }
        }

        return new Config(apiBaseUrl, authToken, fromName, color, patternRooms);
    }

    private String normalizePattern(String pattern) {
        String normalized;
        int firstSlash = pattern.indexOf('/');
        switch (firstSlash) {
            case -1:
                normalized = "/**/" + pattern;
                break;
            case 0:
                normalized = pattern;
                break;
            default:
                normalized = "/" + pattern;
        }
        log.debug("Normalized pattern '" + pattern + "' to '" + normalized + "'");
        return normalized;
    }

    private static class Config {
        private final String apiBaseUrl;
        private final String authToken;
        private final String fromName;
        private final String color;
        private final Multimap<String, String> roomPatterns;

        private Config(String apiBaseUrl, String authToken, String fromName, String color, Multimap<String, String> roomPatterns) {
            this.apiBaseUrl = apiBaseUrl;
            this.authToken = authToken;
            this.fromName = fromName;
            this.color = color;
            this.roomPatterns = roomPatterns;
        }

        public String apiBaseUrl() {
            return apiBaseUrl;
        }

        public String authToken() {
            return authToken;
        }

        public String fromName() {
            return fromName;
        }

        public String color() {
            return color;
        }

        public Multimap<String, String> roomPatterns() {
            return roomPatterns;
        }
    }

}
